import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    tasks: [],
    showTaskMode: 2, // 0 - archive, 1 - active, 2 - all
    searchText: ''
  },
  mutations: {
    setTaskList(state, payload) {
      state.tasks = payload
    },
    setDisplayTaskMode(state, payload) {
      state.showTaskMode = payload
    },
    setTaskSettings(state, payload) {
      state.showTaskMode = payload.showTaskMode
    },
    setSearchText (state, payload) {
      state.searchText = payload
    }
  },
  getters: {
    getTasks (state) {

      let taskByActive = []

      if(state.showTaskMode === 2) {
        taskByActive = state.tasks
      }
      else {
          taskByActive = state.tasks.filter( (task) => {
          return task.active && state.showTaskMode === 1 || !task.active && state.showTaskMode === 0
        })
      }

      if(state.searchText === '') return taskByActive

      return taskByActive.filter( ({text}) => text.includes(state.searchText))
    },
    getTaskById: state => id => {
      return state.tasks.find(task => task.id == id);
    }
  },
  actions: {
    loadTasksFromLocalStorage (context) {
        let payload = JSON.parse(localStorage.getItem('taskList')) || []
        context.commit('setTaskList', payload)
    },
    loadSettings (context) {
      let payload = JSON.parse(localStorage.getItem('taskSettings')) || {showTaskMode: 2}
      context.commit('setTaskSettings', payload)
  },
    addNewTask (context, newTask) {
      let payload = [...this.state.tasks, newTask]
      localStorage.setItem('taskList', JSON.stringify(payload))
      context.commit('setTaskList', payload)
  },

    updateTask (context, updatedTask) {
      let newTasksList = [...this.state.tasks]
      let taskIndex = newTasksList.findIndex( ({id}) => id == updatedTask.id)
      newTasksList[taskIndex] = updatedTask
      localStorage.setItem('taskList', JSON.stringify(newTasksList))
      context.commit('setTaskList', newTasksList)
  },
    setDisplayTaskMode(context, payload) {
      localStorage.setItem('taskSettings', JSON.stringify({showTaskMode: payload}))

      context.commit('setDisplayTaskMode', payload)
    }
  }
})
