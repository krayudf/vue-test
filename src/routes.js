import VueRouter from 'vue-router'
import Home from './pages/Home'
import NewTask from './pages/NewTask'
import FullTask from './pages/FullTask'
import EditTask from './pages/EditTask'


export default new VueRouter({
    routes: [
        {
            path: '/',
            component: Home
        },
        {
            path: '/newtask',
            component: NewTask
        },
        {
            path: '/task/:id',
            component: FullTask
        }
        ,
        {
            path: '/edit/:id',
            component: EditTask
        }
    ],
    mode: 'history'
})
